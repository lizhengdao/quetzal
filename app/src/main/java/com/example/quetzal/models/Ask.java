package com.example.quetzal.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.quetzal.room.RoomConstants;

@Entity(tableName = RoomConstants.tableAsk)
public class Ask {
    @PrimaryKey
    @ColumnInfo(name =RoomConstants.columnAskId)
    private final int askId;

    @NonNull
    @ColumnInfo(name = RoomConstants.columnAskName)
    private final String askName;

    @NonNull
    @ColumnInfo(name = RoomConstants.columnAskCategory)
    private final String askCategory;

    public Ask(int askId, @NonNull String askName, @NonNull String askCategory) {
        this.askId = askId;
        this.askName = askName;
        this.askCategory = askCategory;
    }

    public int getAskId() {
        return askId;
    }

    @NonNull
    public String getAskName() {
        return askName;
    }

    @NonNull
    public String getAskCategory() {
        return askCategory;
    }
}