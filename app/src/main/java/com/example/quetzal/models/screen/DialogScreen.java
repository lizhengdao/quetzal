package com.example.quetzal.models.screen;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

public class DialogScreen extends Screen {
    public DialogScreen(@NonNull String TAG) {
        super(false, TAG);
    }
}
