package com.example.quetzal.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.quetzal.room.RoomConstants;

@Entity(tableName = RoomConstants.tablePunishment)
public class Punishment {
    @PrimaryKey
    @ColumnInfo(name = RoomConstants.columnPunishmentId)
    private final int punishmentId;

    @NonNull
    @ColumnInfo(name = RoomConstants.columnPunishmentName)
    private final String punishmentName;

    @NonNull
    @ColumnInfo(name = RoomConstants.columnPunishmentCategory)
    private final String punishmentCategory;

    @NonNull
    @ColumnInfo(name = RoomConstants.columnPunishmentPicture)
    private final String punishmentPicture;

    @NonNull
    @ColumnInfo(name = RoomConstants.getColumnPunishmentPictureUrl)
    private final String punishmentPictureUrl;


    public Punishment(int punishmentId, @NonNull String punishmentName, @NonNull String punishmentCategory, @NonNull String punishmentPicture, @NonNull String punishmentPictureUrl) {
        this.punishmentId = punishmentId;
        this.punishmentName = punishmentName;
        this.punishmentCategory = punishmentCategory;
        this.punishmentPicture = punishmentPicture;
        this.punishmentPictureUrl = punishmentPictureUrl;
    }

    public int getPunishmentId() {
        return punishmentId;
    }

    @NonNull
    public String getPunishmentName() {
        return punishmentName;
    }

    @NonNull
    public String getPunishmentCategory() {
        return punishmentCategory;
    }

    @NonNull
    public String getPunishmentPicture() {
        return punishmentPicture;
    }

    @NonNull
    public String getPunishmentPictureUrl() {
        return punishmentPictureUrl;
    }
}
