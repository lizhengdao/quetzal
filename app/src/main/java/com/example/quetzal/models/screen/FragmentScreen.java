package com.example.quetzal.models.screen;

import androidx.annotation.NonNull;

public class FragmentScreen extends Screen {
    public FragmentScreen(boolean networkRequired,@NonNull String TAG) {
        super(networkRequired, TAG);
    }
}
