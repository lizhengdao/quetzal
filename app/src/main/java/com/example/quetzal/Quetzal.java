package com.example.quetzal;

import android.app.Application;
/**
 * Created by Andrés Rodríguez on 06/11/2020.
 */
public class Quetzal extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //Application Class
        ImageManager.Companion.getInstance().initialize(this);
    }
}
