package com.example.quetzal.interfaces;

public interface RoomListener<T> {
    void onFinish(T result);
}
