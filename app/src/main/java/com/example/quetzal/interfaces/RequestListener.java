package com.example.quetzal.interfaces;

public interface RequestListener<T> {
    default void onStar(){}
    default void onSuccess(T response){}
    default void onError(Exception ex){}
}
