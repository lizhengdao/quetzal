package com.example.quetzal.viewmodels;

import android.app.Application;

import com.example.quetzal.models.Ask;
import com.example.quetzal.repositories.AskRepository;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class AskViewModel extends AndroidViewModel {
    private final AskRepository repository;

    private LiveData<Ask> ask;

    private final LiveData<List<Ask>> asks;
    private LiveData<String> askCategory;

    public AskViewModel(Application application) {
        super(application);

        this.repository = AskRepository.getInstance(application);
        this.asks = repository.LoadAsks();
        this.ask = repository.getAsk();
    }

    public LiveData<Ask> getAsk() {
        return ask;
    }

    public void getAskById(int id) {
        repository.getAsksById(id);
    }

    public LiveData<List<Ask>> loadAsks() {
        return asks;
    }
}

