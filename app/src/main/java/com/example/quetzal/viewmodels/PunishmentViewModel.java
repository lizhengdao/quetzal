package com.example.quetzal.viewmodels;

import android.app.Application;

import com.example.quetzal.models.Punishment;
import com.example.quetzal.repositories.PunishmentRepository;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class PunishmentViewModel extends AndroidViewModel {
    private final PunishmentRepository repository;

    private final LiveData<Punishment> punishment;

    public PunishmentViewModel(Application application) {
        super(application);

        this.repository = PunishmentRepository.getInstance(application);
        this.punishment = repository.getPunishment();
    }

    public LiveData<Punishment> getPunishment (){
        return punishment;
    }

    public void getPunishmentCategory(String category){
        repository.getRandomPunishmentByCategory(category);
    }

}
