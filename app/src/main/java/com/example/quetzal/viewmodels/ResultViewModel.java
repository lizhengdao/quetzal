package com.example.quetzal.viewmodels;

import android.app.Application;

import com.example.quetzal.repositories.ResultRepository;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

/**
 * Created by Andrés Rodríguez on 15/10/2020.
 */
public class ResultViewModel extends AndroidViewModel {
    private final ResultRepository repository;

    private final LiveData<String> result1;
    private final LiveData<String> result2;
    private final LiveData<String> result3;

    public ResultViewModel(Application application){
        super(application);

        this.repository = ResultRepository.getInstance(application);
        this.result1 = repository.getResult1();
        this.result2 = repository.getResult2();
        this.result3 = repository.getResult3();
    }

    public LiveData<String> getResult1() {
        return result1;
    }

    public LiveData<String> getResult2() {
        return result2;
    }

    public LiveData<String> getResult3() {
        return result3;
    }

    public void generalResult(String option1, String option2, String option3, String category) {
        repository.generalResult(option1, option2, option3, category);
    }
}
