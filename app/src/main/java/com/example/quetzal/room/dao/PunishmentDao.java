package com.example.quetzal.room.dao;

import com.example.quetzal.models.Punishment;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface PunishmentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Punishment punishment);

    @Update
    void update(Punishment punishment);

    @Delete
    void delete(Punishment punishment);

    @Query("DELETE FROM tablePunishment")
    void deleteAll();

    @Query("SELECT * FROM tablePunishment WHERE columnPunishmentId= :punishmentId")
    Punishment getPunishmentById(int punishmentId);

    @Query("SELECT * FROM tablePunishment WHERE columnPunishmentCategory = :punishmentCategory")
    List<Punishment> getPunishmentsByCategory(String punishmentCategory);

    @Query("SELECT * FROM tablePunishment ORDER BY columnPunishmentId DESC")
    LiveData<List<Punishment>> getAllPunishment();

    @Query("SELECT * FROM tablePunishment WHERE columnPunishmentCategory = :punishmentCategory ORDER BY RANDOM() LIMIT 1")
    Punishment getRandomPunishmentsByCategory(String punishmentCategory);

}