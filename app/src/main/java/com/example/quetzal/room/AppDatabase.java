package com.example.quetzal.room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.quetzal.asynctasks.PopulateDbAsyncTask;
import com.example.quetzal.models.Ask;
import com.example.quetzal.models.Punishment;
import com.example.quetzal.room.dao.AskDao;
import com.example.quetzal.room.dao.PunishmentDao;

@Database(entities = {Ask.class, Punishment.class},version = RoomConstants.databaseVersion)
public abstract class AppDatabase extends RoomDatabase {
    private static volatile AppDatabase instance;

    public abstract AskDao askDao();
    public abstract PunishmentDao punishmentDao();

    public static synchronized AppDatabase getInstance(Context context) {
        if(instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, RoomConstants.databaseName)
                    .addCallback(roomCallback)
                    .build();
        }
        return  instance;
    }

    private static final RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };
}
