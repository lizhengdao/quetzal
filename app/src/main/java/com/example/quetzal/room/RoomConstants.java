package com.example.quetzal.room;

public class RoomConstants {
    public static final String databaseName = "Quetzal.db";
    public static final int databaseVersion = 1;

    //Table Names
    public static final String tableAsk = "tableAsk";
    public static final String tablePunishment = "tablePunishment";

    //Table Ask
    public static final String columnAskId = "columnAskId";
    public static final String columnAskName = "columnAskName";
    public static final String columnAskCategory = "columnAskCategory";

    //Table Punishment
    public static final String columnPunishmentId = "columnPunishmentId";
    public static final String columnPunishmentName = "columnPunishmentName";
    public static final String columnPunishmentCategory = "columnPunishmentCategory";
    public static final String columnPunishmentPicture = "columnPunishmentPicture";
    public static final String getColumnPunishmentPictureUrl = "columnPunishmentPictureUrl";




}
