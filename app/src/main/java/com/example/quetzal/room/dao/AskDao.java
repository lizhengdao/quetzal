package com.example.quetzal.room.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.quetzal.models.Ask;

import java.util.List;

@Dao
public interface AskDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Ask ask);

    @Update
    void update(Ask ask);

    @Delete
    void delete(Ask ask);

    @Query("DELETE FROM tableAsk")
    void deleteAll();

    @Query("SELECT * FROM tableAsk WHERE columnAskId= :askId LIMIT 1")
    Ask getAskById(int askId);

    @Query("SELECT * FROM tableAsk WHERE columnAskCategory = :askCategory")
    LiveData<List<Ask>> getAsksByCategory(String askCategory);

    @Query("SELECT * FROM tableAsk ORDER BY columnAskId")
    LiveData<List<Ask>> getAllAsks();

}
