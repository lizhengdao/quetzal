package com.example.quetzal.asynctasks;

import android.os.AsyncTask;

import com.example.quetzal.interfaces.RoomListener;
import com.example.quetzal.models.Punishment;
import com.example.quetzal.room.dao.PunishmentDao;

import java.util.List;

public class GetRandomPunishmentByCategory  extends AsyncTask<Void, Void, Punishment> {
    PunishmentDao punishmentDao;

    private final String category;

    private final RoomListener<Punishment> listener;

    public GetRandomPunishmentByCategory (PunishmentDao punishmentDao , String category, RoomListener<Punishment> listener) {
        this.punishmentDao = punishmentDao;
        this.category = category;
        this.listener = listener;
    }

    @Override
    protected Punishment doInBackground(Void... voids) {
        return punishmentDao.getRandomPunishmentsByCategory(category);
    }

    @Override
    protected void onPostExecute(Punishment punishment) {
        if ( punishment != null){
            listener.onFinish(punishment);
        }
    }
}
