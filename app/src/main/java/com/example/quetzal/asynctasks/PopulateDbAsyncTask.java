package com.example.quetzal.asynctasks;

import android.os.AsyncTask;

import com.example.quetzal.models.Ask;
import com.example.quetzal.models.Punishment;
import com.example.quetzal.room.AppDatabase;
import com.example.quetzal.room.dao.AskDao;
import com.example.quetzal.room.dao.PunishmentDao;

public class PopulateDbAsyncTask extends AsyncTask<Void,Void,Void> {
    private AskDao askDao;
    private PunishmentDao punishmentDao;

    public PopulateDbAsyncTask(AppDatabase db) {
        this.askDao = db.askDao();
        this.punishmentDao = db.punishmentDao();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        //Table Dao
        askDao.insert(new Ask(1, "With who are you going to marry?", "A"));
        askDao.insert(new Ask(2, "In what transport will you go on your honeymoon?", "B"));
        askDao.insert(new Ask(3, "Where will your honeymoon be??", "C"));
        askDao.insert(new Ask(4, "How many children are you going tu have?", "D"));

        //Table Punishment
        punishmentDao.insert(new Punishment(1,"Ludovico Peluche", "A","ludo", "https://media1.tenor.com/images/0425f1812662002dd7e7677c9a0883ba/tenor.gif?itemid=4997467"));
        punishmentDao.insert(new Punishment(2,"Peña Nieto", "A","pena", "https://media1.tenor.com/images/dce2e9f1de33ba18ad6543b1d9af6320/tenor.gif?itemid=12543511"));
        punishmentDao.insert(new Punishment(3,"Bob esponga", "A","bob", "https://media1.tenor.com/images/d32ba77e8ea31ab75703803c4a5fbc8d/tenor.gif?itemid=14715895"));
        punishmentDao.insert(new Punishment(4,"En una Vaca", "B","vaca", "https://media1.tenor.com/images/3cd97a9f9a605b3fbf50ba1bd6c1192e/tenor.gif?itemid=4536864"));
        punishmentDao.insert(new Punishment(5,"En un Burro", "B","burro", "https://media1.tenor.com/images/6f4fa5fea73897955d4b0508c47eeca5/tenor.gif?itemid=14645687"));
        punishmentDao.insert(new Punishment(6,"Caminando miando", "B","caminando", "https://media1.tenor.com/images/edcd688235072f2ce58869a93b7240d8/tenor.gif?itemid=4597925"));
        punishmentDao.insert(new Punishment(7,"En Un Perreke", "C","perreke", "https://media1.tenor.com/images/33fd97a3fa1dfdd80ac9dd53b6dc3c10/tenor.gif?itemid=14818214"));
        punishmentDao.insert(new Punishment(8,"En NezaYork", "C","neza", "https://i.makeagif.com/media/12-12-2018/XStEUW.gif"));
        punishmentDao.insert(new Punishment(9,"En un basurero", "C","imagenbasurero", "https://media.tenor.com/images/e7e8a1480e446040b181b7a19dddedd1/tenor.gif"));
        punishmentDao.insert(new Punishment(10,"Tendrás una docena", "D","docena", "https://media.giphy.com/media/qno45kiHotT1K/giphy.gif"));
        punishmentDao.insert(new Punishment(11,"No tendrás hijos, tendrás un Dinosaurio", "D","dino", "https://media.tenor.com/images/ddce5a0dbe768e965abee0cc891d3db2/tenor.gif"));
        punishmentDao.insert(new Punishment(12,"No tendrás hijos, tendrás un gato volador", "D","gato", "https://memeschistosos.net/wp-content/uploads/2016/02/Gifs-de-Gatos-Chistosos22.gif"));
        return null;
    }
}
