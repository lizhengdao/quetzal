package com.example.quetzal.asynctasks;

import android.os.AsyncTask;

import com.example.quetzal.interfaces.RoomListener;
import com.example.quetzal.models.Punishment;
import com.example.quetzal.room.dao.PunishmentDao;

import java.util.List;

public class GetPunishmentsCategories extends AsyncTask<Void, Void, List<Punishment>> {
    PunishmentDao punishmentDao;

    private final String category;

    private final RoomListener<List<Punishment>> listener;

    public GetPunishmentsCategories (PunishmentDao punishmentDao, String category, RoomListener<List<Punishment>> listener) {
        this.punishmentDao = punishmentDao;
        this.category = category;
        this.listener = listener;
    }

    @Override
    protected List<Punishment> doInBackground(Void... voids) {
        return punishmentDao.getPunishmentsByCategory(category);
    }

    @Override
    protected void onPostExecute(List<Punishment> punishments) {
        if (punishments != null) {
            listener.onFinish(punishments);
        }
    }
}
