package com.example.quetzal.ui.fragments;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quetzal.R;
import com.example.quetzal.base.BaseFragment;
import com.example.quetzal.databinding.FragmentUserFormBinding;
import com.example.quetzal.models.screen.FragmentScreen;
import com.example.quetzal.models.screen.Screen;
import com.example.quetzal.ui.activities.MainActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;

public class UserFormFragment extends BaseFragment {
    private final Screen screen = new FragmentScreen(false, "UserFormFragment");
    private FragmentUserFormBinding binding;

    private AnimationDrawable animationBtnPlay, animationBtnBack;

    @Override
    protected Screen getScreen() {
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentUserFormBinding.inflate(inflater, container, false);
        //Animation button play
        animationBtnPlay =(AnimationDrawable) binding.btnOkay.getBackground();
        animationBtnPlay.setEnterFadeDuration(1500);
        animationBtnPlay.setExitFadeDuration(1500);
        //Animation button back
        animationBtnBack = (AnimationDrawable) binding.btnExit.getBackground();
        animationBtnBack.setEnterFadeDuration(1500);
        animationBtnBack.setExitFadeDuration(1500);
        return binding.getRoot();

    }

    @Override

    protected void prepareComponents() {


        binding.btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextActivityQuestion();
            }
        });

        binding.btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }


    private void nextActivityQuestion() {
        String name = String.valueOf(binding.etName.getText());
        String edge = String.valueOf(binding.etAge.getText());

        if (name.isEmpty()) {
            showToast("Name is empty");
            binding.etName.setFocusable(true);
        } else if (edge.isEmpty()){
            showToast("Edge is empty");
            binding.etAge.setFocusable(true);
        } else {
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_userFormFragment_to_ask1Fragment);
        }
    }
}