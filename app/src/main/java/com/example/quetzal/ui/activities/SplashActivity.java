package com.example.quetzal.ui.activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.media.MediaParser;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.quetzal.R;
import com.example.quetzal.base.BaseActivity;
import com.example.quetzal.databinding.ActivitySplashBinding;
import com.example.quetzal.models.screen.ActivityScreen;
import com.example.quetzal.models.screen.Screen;

import androidx.viewbinding.ViewBinding;

public class SplashActivity extends BaseActivity {
    Screen screen = new ActivityScreen(false, "Activity Splash");

    private ActivitySplashBinding binding;

   private CharSequence charSequence;
   private int index;
   private final long delay = 200;
   private final Handler handler = new Handler();

    @Override
    protected Screen getScreen() {
        return screen;
    }

    @Override
    protected ViewBinding getViewBinding() {
        if (binding == null) {
            binding = ActivitySplashBinding.inflate(getLayoutInflater());
        }
        return binding;
    }

    @Override
    protected void prepareComponents() {
        //Sound
        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.sonidocorazon);
        mediaPlayer.start();

        //Set full Screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
                , WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Initialize top animation
        Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.top_wave);
        //Start top animation
        binding.ivTop.setAnimation(animation1);

        //Initialize object animator
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(binding.ivHeart
                , PropertyValuesHolder.ofFloat("scaleX", 1.2f)
                , PropertyValuesHolder.ofFloat("scaleY", 1.2f));

        //set duration
        objectAnimator.setDuration(500);
        //Set repeat count
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        //Set repeat mode
        objectAnimator.setRepeatMode(ValueAnimator.REVERSE);
        //Start animator
        objectAnimator.start();

        //Set animate text
        animateText("Plane of Love");

        //Load Gif Animation to heart beat
        Glide
                .with(this)
                .load(R.drawable.heart_beat)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.ivHeartBeat);

        //Initialize bottom animation
        Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.bottom_wave);
        //Star bottom animation
        binding.ivBottom.setAnimation(animation2);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                mediaPlayer.stop();
                finish();
            }
        }, 4000);

    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //When runnable is run
            //Set text
            binding.textView.setText(charSequence.subSequence(0, index++));
            //check condition
            if (index <= charSequence.length()) {
                //When index is equal to text lengh
                //Run handler
                handler.postDelayed(runnable, delay);
            }
        }
    };

    //Create animated text method
    public void animateText(CharSequence cs) {
        //Set text
        charSequence = cs;
        //Clear index
        index = 0;
        //Text clear
        binding.textView.setText("");
        //Remove call back
        handler.removeCallbacks(runnable);
        //Run handler
        handler.postDelayed(runnable, delay);
    }
}