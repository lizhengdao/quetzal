package com.example.quetzal.ui.fragments;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quetzal.R;
import com.example.quetzal.base.NavigationFragment;
import com.example.quetzal.databinding.FragmentResultBinding;
import com.example.quetzal.models.screen.FragmentScreen;
import com.example.quetzal.models.screen.Screen;
import com.example.quetzal.ui.activities.ContainerActivity;
import com.example.quetzal.utils.LogUtils;
import com.example.quetzal.viewmodels.ResultViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

public class ResultFragment extends NavigationFragment {
    Screen screen = new FragmentScreen(false, "Result Fragment");

    FragmentResultBinding binding;

    MediaPlayer mediaPlayer;
    @Override
    protected Screen getScreen() {
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       binding = FragmentResultBinding.inflate(inflater, container, false);
       return binding.getRoot();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mediaPlayer.start();
    }

    @Override
    protected void prepareComponents() {
        //Set music
        mediaPlayer = MediaPlayer.create(getContext(), R.raw.sonidomarchanubcial);
        mediaPlayer.start();
        ResultViewModel viewModel;
        viewModel = new ViewModelProvider(this).get(ResultViewModel.class);

        binding.btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ContainerActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        viewModel.getResult1().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String result) {
                LogUtils.print("DATA_RECEIVED: " + result);
                binding.txResult1.setText(result);
            }
        });

        viewModel.getResult2().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String result) {
                binding.txResult2.setText(result);
            }
        });

        viewModel.getResult3().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String result) {
                LogUtils.print("DATA_FROM_GENERAL_RESULT" + result );
                binding.txResult3.setText(result);
            }
        });
    }
}