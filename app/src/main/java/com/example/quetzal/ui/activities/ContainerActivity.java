package com.example.quetzal.ui.activities;

import com.example.quetzal.base.BaseActivity;
import com.example.quetzal.databinding.ActivityContainerBinding;
import com.example.quetzal.models.screen.ActivityScreen;
import com.example.quetzal.models.screen.Screen;
import com.example.quetzal.viewmodels.AskViewModel;

import androidx.lifecycle.ViewModelProvider;
import androidx.viewbinding.ViewBinding;

import static com.example.quetzal.utils.Constants.ASK_1;

public class ContainerActivity extends BaseActivity {
    Screen screen = new ActivityScreen(false, "ContainerActivity");

    private ActivityContainerBinding binding;

    @Override
    protected Screen getScreen() {
        return screen;
    }

    @Override
    protected ViewBinding getViewBinding() {
        if (binding == null) {
            binding = ActivityContainerBinding.inflate(getLayoutInflater());
        }

        return binding;

    }

    @Override
    protected void prepareComponents() {
        AskViewModel viewModel;
        viewModel = new ViewModelProvider(this).get(AskViewModel.class);

        viewModel.getAskById(ASK_1);
    }

    @Override
    public void onBackPressed() {
    }
}