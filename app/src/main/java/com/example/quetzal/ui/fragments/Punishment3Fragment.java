package com.example.quetzal.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quetzal.ImageManager;
import com.example.quetzal.base.NavigationFragment;
import com.example.quetzal.databinding.FragmentPunishment3Binding;
import com.example.quetzal.models.Punishment;
import com.example.quetzal.models.screen.FragmentScreen;
import com.example.quetzal.models.screen.Screen;
import com.example.quetzal.utils.LogUtils;
import com.example.quetzal.viewmodels.PunishmentViewModel;
import com.example.quetzal.viewmodels.ResultViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import static com.example.quetzal.utils.Constants.CATEGORY_C;

public class Punishment3Fragment extends NavigationFragment {
    private final Screen screen = new FragmentScreen(false  ,"Punishment 3");

    private FragmentPunishment3Binding binding;

    @Override
    protected Screen getScreen() {
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPunishment3Binding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void prepareComponents() {
        String option1 = getOption1();
        String option2 = getOption2();
        final String[] txPunishment = {""};
        LogUtils.print("Options: " + option1 +" "+ option2);

        PunishmentViewModel viewModel;
        viewModel = new ViewModelProvider(this).get(PunishmentViewModel.class);
        ResultViewModel viewModelResult = new ViewModelProvider(this).get(ResultViewModel.class);

        binding.btnNextQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openResult(binding);
                viewModelResult.generalResult(option1,option2, txPunishment[0], CATEGORY_C);
            }
        });

        viewModel.getPunishment().observe(this, new Observer<Punishment>() {
            @Override
            public void onChanged(Punishment punishment) {
                if (punishment != null) {
                    txPunishment[0] = punishment.getPunishmentName();
                    String pictureUrl = punishment.getPunishmentPictureUrl();

                    binding.txPunishment3.setText(txPunishment[0]);
                    LogUtils.print("PictureUrl: " + pictureUrl);
                    ImageManager.Companion.getInstance().setImage(pictureUrl, binding.ivPunishment1);
                }
            }
        });

        viewModel.getPunishmentCategory(CATEGORY_C);

    }
}