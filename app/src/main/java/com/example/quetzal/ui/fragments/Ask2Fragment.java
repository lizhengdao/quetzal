package com.example.quetzal.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quetzal.base.NavigationFragment;
import com.example.quetzal.databinding.FragmentAsk2Binding;
import com.example.quetzal.models.Ask;
import com.example.quetzal.models.screen.FragmentScreen;
import com.example.quetzal.models.screen.Screen;
import com.example.quetzal.utils.LogUtils;
import com.example.quetzal.viewmodels.AskViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import static com.example.quetzal.utils.Constants.ASK_2;

public class Ask2Fragment extends NavigationFragment {
    private final Screen screen = new FragmentScreen(false, "Ask2");

    private FragmentAsk2Binding binding;

    private AskViewModel viewModel;

    @Override
    protected Screen getScreen() {
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentAsk2Binding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void prepareComponents() {
        viewModel = new ViewModelProvider(this).get(AskViewModel.class);

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextActivity();
            }
        });

        viewModel.getAsk().observe(this, new Observer<Ask>() {
            @Override
            public void onChanged(Ask ask) {
                if (ask != null) {
                    binding.txQuestion2.setText(ask.getAskName());
                    LogUtils.print("AskInformation : " + ask.getAskName());
                }
            }
        });

        viewModel.getAskById(ASK_2);
    }

    private void nextActivity() {
        String option1 = String.valueOf(binding.etName1.getText());
        String option2 = String.valueOf(binding.etName2.getText());

        if (option1.isEmpty()) {
            showToast("option 1 is empty");
        } else if (option2.isEmpty()) {
            showToast("option 2 is empty");
        } else {
            openPunishment2(option1, option2, binding);
        }
    }
}