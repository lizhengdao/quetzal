package com.example.quetzal.ui.activities;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.quetzal.R;
import com.example.quetzal.base.BaseActivity;
import com.example.quetzal.databinding.ActivityMainBinding;
import com.example.quetzal.models.screen.ActivityScreen;
import com.example.quetzal.models.screen.Screen;

import androidx.viewbinding.ViewBinding;

public class MainActivity extends BaseActivity {
private final Screen screen = new ActivityScreen(false,"MainActivity");

private ActivityMainBinding binding;
private AnimationDrawable animationBtnExit, animationBtnPlay, animationBtnSettings;

    @Override
    protected Screen getScreen() {
        return screen;
    }

    @Override
    protected ViewBinding getViewBinding() {
        if (binding == null) {
            binding = ActivityMainBinding.inflate(getLayoutInflater());
        }

        return binding;
    }

    @Override
    protected void prepareComponents() {
        //StarAnimation
        //Animation btnExit
        animationBtnExit = (AnimationDrawable) binding.btnExit.getBackground();
        animationBtnExit.setEnterFadeDuration(1500);
        animationBtnExit.setExitFadeDuration(1500);
        //Animation btnPlay
        animationBtnPlay = (AnimationDrawable) binding.btnPlay.getBackground();
        animationBtnPlay.setEnterFadeDuration(1500);
        animationBtnPlay.setExitFadeDuration(1500);
        //Animation btbSettings
        animationBtnSettings = (AnimationDrawable) binding.btSettings.getBackground();
        animationBtnSettings.setEnterFadeDuration(1500);
        animationBtnSettings.setExitFadeDuration(1500);



        Glide.with(this)
                .load(R.raw.imagenamor)
                .into(binding.ivMenuPicture);

      binding.btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(MainActivity.this, ContainerActivity.class);
                startActivity(mIntent);
                finish();
            }
        });

      binding.btnExit.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              finish();
          }
      });
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(animationBtnExit != null && !animationBtnExit.isRunning() && animationBtnPlay != null && !animationBtnPlay.isRunning()
                && animationBtnSettings != null && !animationBtnSettings.isRunning()){
            animationBtnExit.start();
            animationBtnPlay.start();
            animationBtnSettings.start();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationBtnExit != null && animationBtnExit.isRunning() && animationBtnPlay != null && animationBtnPlay.isRunning()
                && animationBtnSettings != null && animationBtnSettings.isRunning()) {
            animationBtnExit.stop();
            animationBtnPlay.stop();
            animationBtnSettings.stop();
        }
    }
}