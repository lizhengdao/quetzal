package com.example.quetzal.repositories;

import android.app.Application;

import com.example.quetzal.asynctasks.GetRandomPunishmentByCategory;
import com.example.quetzal.interfaces.RoomListener;
import com.example.quetzal.models.Punishment;
import com.example.quetzal.room.AppDatabase;
import com.example.quetzal.room.dao.PunishmentDao;
import com.example.quetzal.utils.LogUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class PunishmentRepository {
    public static volatile PunishmentRepository instance;

    public final PunishmentDao punishmentDao;
    private final MutableLiveData<Punishment> punishment = new MutableLiveData<>();

    private PunishmentRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application.getApplicationContext());
        this.punishmentDao = database.punishmentDao();

        this.punishment.setValue(null);
    }

    public static PunishmentRepository getInstance(Application application) {
        if(instance == null) {
            synchronized (PunishmentRepository.class){
                if(instance ==null ) {
                    instance = new PunishmentRepository(application);
                }
            }
        }
        return instance;
    }

    @NonNull
    public LiveData<Punishment> getPunishment() {
        return punishment;
    }

    public void setPunishment(Punishment punishment) {
        this.punishment.setValue(punishment);
        this.punishment.postValue(null);
    }

    public void getRandomPunishmentByCategory(String category) {
        LogUtils.print("CATEGORY_ENTERED: " + category);

        new GetRandomPunishmentByCategory(punishmentDao, category, new RoomListener<Punishment>() {
            @Override
            public void onFinish(Punishment result) {
                setPunishment(result);
                LogUtils.print("RANDOM_PUNISHMENT: " + result.getPunishmentId() + " " + result.getPunishmentName());

            }
        }).execute();
    }
}