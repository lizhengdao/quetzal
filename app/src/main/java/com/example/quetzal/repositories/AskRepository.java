package com.example.quetzal.repositories;

import android.app.Application;

import com.example.quetzal.asynctasks.GetAsksByIdAsyncTask;
import com.example.quetzal.interfaces.RoomListener;
import com.example.quetzal.models.Ask;
import com.example.quetzal.room.AppDatabase;
import com.example.quetzal.room.dao.AskDao;
import com.example.quetzal.utils.LogUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class AskRepository {
    public static volatile AskRepository instance;

    @NonNull
    private final AskDao askDao;

    private final LiveData<List<Ask>> asks;
    private final MutableLiveData<Ask> ask = new MutableLiveData<>();

    private AskRepository(Application application) {
       AppDatabase database = AppDatabase.getInstance(application.getApplicationContext());
        this.askDao = database.askDao();

        asks = askDao.getAllAsks();
        this.ask.setValue(null);
    }

    public static AskRepository getInstance(@NonNull Application application) {
        if (instance == null) {
            synchronized (AskRepository.class) {
                if (instance == null) {
                    instance = new AskRepository(application);
                }
            }
        }

        return instance;

    }

    @NonNull
    public LiveData<Ask> getAsk() {
        return ask;
    }

    private void setAsk(@NonNull Ask ask) {
        this.ask.setValue(ask);
        this.ask.postValue(null);
    }

    public void getAsksById(int askId) {
        new GetAsksByIdAsyncTask(askDao, askId, new RoomListener<Ask>() {
            @Override
            public void onFinish(Ask ask) {
                setAsk(ask);
                LogUtils.print("AskId: " + ask.getAskId());
            }
        }).execute();
    }

    public LiveData<List<Ask>> LoadAsks() {
        return asks;
    }
}
