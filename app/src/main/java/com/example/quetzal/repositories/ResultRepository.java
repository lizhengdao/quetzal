package com.example.quetzal.repositories;

import android.app.Application;

import com.example.quetzal.utils.LogUtils;

import java.util.Random;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import static com.example.quetzal.utils.Constants.CATEGORY_A;
import static com.example.quetzal.utils.Constants.CATEGORY_B;
import static com.example.quetzal.utils.Constants.CATEGORY_C;

/**
 * Created by Andrés Rodríguez on 15/10/2020.
 */
public class ResultRepository {
    public static volatile ResultRepository instance;

    public final MutableLiveData<String> result1 = new MutableLiveData<>();
    public final MutableLiveData<String> result2 = new MutableLiveData<>();
    public final MutableLiveData<String> result3 = new MutableLiveData<>();

    private ResultRepository(Application application) {
    }

    public static ResultRepository getInstance(Application application) {
        if (instance == null) {
            synchronized (ResultRepository.class) {
                if (instance == null) {
                    instance = new ResultRepository(application);
                }
            }
        }
        return instance;
    }

    public LiveData<String> getResult1() {
        return result1;
    }

    public LiveData<String> getResult2() {
        return result2;
    }

    public LiveData<String> getResult3() {
        return result3;
    }

    public void setResult1(String result1) {
        this.result1.setValue(result1);
        this.result1.postValue(result1);
    }

    public void setResult2(String result2) {
        this.result2.setValue(result2);
        this.result2.postValue(result2);
    }

    public void setResult3(String result3) {
        this.result3.setValue(result3);
        this.result3.postValue(result3);
    }

    public void generalResult(String option1, String option2, String option3, String category) {
        LogUtils.print("3OPTIONS_RECEIVED: " + option1 + option2 + option3);
        String result;
        switch (category) {
            case CATEGORY_A: {
                result = calculateResult(option1, option2, option3);
                setResult1(result);
                LogUtils.print("DATA_FROM_GENERAL_RESULT: " + result );
                break;
            }
            case CATEGORY_B: {
                result = calculateResult(option1, option2, option3);
                setResult2(result);
                LogUtils.print("DATA_FROM_GENERAL_RESULT: " + result );
                break;
            }
            case CATEGORY_C: {
                result = calculateResult(option1, option2, option3);
                setResult3(result);
                LogUtils.print("DATA_FROM_GENERAL_RESULT " + result );
                break;
            }
        }
    }

    public String calculateResult(String option1, String option2, String option3) {
        Random random = new Random();
        int selection = random.nextInt(3);

        if (selection == 0) {
            LogUtils.print("The option :" + option1 + " " + "Was Added to Result");
            return option1;
        } else if (selection == 1) {
            LogUtils.print("The option :" + option2 + " " + "Was Added to Result");
            return option2;
        } else if (selection == 2) {
            LogUtils.print("The option :" + option3 + " " + "Was Added to Result");
            return option3;
        } else {
            LogUtils.print("Error option no exist");
        }
        return "Error";
    }
}
