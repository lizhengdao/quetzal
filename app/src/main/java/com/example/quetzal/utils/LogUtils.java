package com.example.quetzal.utils;

import android.util.Log;

import com.example.quetzal.BuildConfig;

public class LogUtils {
    public static void print(String text) {
        if(BuildConfig.DEBUG) {
            if(text != null && !text.isEmpty()) {
                Log.d("Andy", text);
            }
        }
    }

    public static void print(String TAG, String text) {
        if(BuildConfig.DEBUG) {
            if(text != null && !text.isEmpty()) {
                Log.d(TAG, text);
            }
        }
    }

    public static void print(String TAG, Exception ex) {
        if(BuildConfig.DEBUG) {
            Log.d("Andy", TAG + " -> " + ex.toString());
        }
    }

}
