package com.example.quetzal.utils;

public class Constants {
    // Questions
    public static final int ASK_1 = 1;
    public static final int ASK_2 = 2;
    public static final int ASK_3 = 3;
    public static final int ASK_4 = 4;

    // Punishment categories
    public static final String CATEGORY_A = "A";
    public static final String CATEGORY_B = "B";
    public static final String CATEGORY_C = "C";
    public static final String CATEGORY_D = "D";

    // Bundle
    public static final String OPTION_1 = "OPTION1";
    public static final String OPTION_2 = "OPTION2";

}
