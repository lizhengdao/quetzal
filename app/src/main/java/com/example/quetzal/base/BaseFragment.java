package com.example.quetzal.base;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.quetzal.models.screen.Screen;
import com.example.quetzal.utils.LogUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

public abstract class BaseFragment extends Fragment {
    private boolean available = true;

    public BaseFragment(){
        super();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        LogUtils.print("FragmentName: " + getScreen().getTAG());
        prepareComponents();

    }

    protected abstract Screen getScreen();
    protected abstract void prepareComponents();

    @NonNull
    private FragmentActivity getViewActivity() {
        return requireActivity();
    }

    public void showToast(String message) {
        Toast.makeText(getViewActivity(), message, Toast.LENGTH_SHORT).show();
    }
}