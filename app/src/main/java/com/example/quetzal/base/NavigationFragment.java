package com.example.quetzal.base;

import android.os.Bundle;

import com.example.quetzal.R;
import com.example.quetzal.databinding.FragmentAsk1Binding;
import com.example.quetzal.databinding.FragmentAsk2Binding;
import com.example.quetzal.databinding.FragmentAsk3Binding;
import com.example.quetzal.databinding.FragmentPunishment1Binding;
import com.example.quetzal.databinding.FragmentPunishment2Binding;
import com.example.quetzal.databinding.FragmentPunishment3Binding;

import androidx.annotation.NonNull;

import androidx.navigation.Navigation;

import static com.example.quetzal.utils.Constants.OPTION_1;
import static com.example.quetzal.utils.Constants.OPTION_2;

/**
 * Created by Andrés Rodríguez on 14/10/2020.
 */
public abstract class NavigationFragment extends BaseFragment {

    protected void setOptions(@NonNull Bundle bundle, String option1, String option2){
        bundle.putString(OPTION_1, option1);
        bundle.putString(OPTION_2, option2);
    }

    protected String getOption1() {
        if(getArguments() != null) {
            return getArguments().getString(OPTION_1);
        }
        return "empty";
    }
    protected String getOption2() {
        if(getArguments() != null) {
            return getArguments().getString(OPTION_2);
        }
        return "empty";
    }

    protected void openPunishment1(String option1, String option2, @NonNull FragmentAsk1Binding binding) {
        Bundle bundle = new Bundle();

        setOptions(bundle, option1, option2);

        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_ask1Fragment_to_punishment1Fragment, bundle);
    }

    protected void openAsk2(@NonNull FragmentPunishment1Binding binding) {
        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_punishment1Fragment_to_ask2Fragment);
    }

    protected void openPunishment2(String option1, String option2, @NonNull FragmentAsk2Binding binding) {
        Bundle bundle = new Bundle();

        setOptions(bundle, option1, option2);

        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_ask2Fragment_to_punishment2Fragment, bundle);
    }

    protected void openAsk3(@NonNull FragmentPunishment2Binding binding) {
        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_punishment2Fragment_to_ask3Fragment);
    }

    protected void openPunishment3(String option1, String option2, @NonNull FragmentAsk3Binding binding) {
        Bundle bundle = new Bundle();

        setOptions(bundle, option1, option2);

        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_ask3Fragment_to_punishment3Fragment, bundle);
    }

    protected void openResult(@NonNull FragmentPunishment3Binding binding) {
        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_punishment3Fragment_to_resultFragment);
    }
}
