package com.example.quetzal.base;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewbinding.ViewBinding;

import com.example.quetzal.utils.LogUtils;
import com.example.quetzal.R;
import com.example.quetzal.models.screen.Screen;
import com.google.android.material.appbar.AppBarLayout;

public abstract class BaseActivity extends AppCompatActivity {
    protected TextView tvTitle;
    protected AppBarLayout appBarLayout;
    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getViewBinding().getRoot());
        init();
        prepareComponents();
    }
    protected abstract Screen getScreen();
    protected abstract ViewBinding getViewBinding();
    protected abstract void prepareComponents();

    private void init() {
        appBarLayout = findViewById(R.id.appBarLayout);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void setTitle(String title) {
        if(tvTitle != null) {
            tvTitle.setText(title);
        }
    }


    protected void toolbarVisibility(boolean visibility) {
        if(appBarLayout != null) {
            if(visibility) {
                Visible(appBarLayout);
            } else {
                Gone(appBarLayout);
            }
        }
    }

    protected void Visible(View view) {
        if(view == null) {
            LogUtils.print("View are null (Visible - BaseActivity)");
            return;
        }

        if(view.getVisibility() != View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
        }
    }


    protected void Gone(View view) {
        if(view == null) {
            LogUtils.print("View are null (Gone - BaseActivity)");
            return;
        }

        if(view.getVisibility() != View.GONE) {
            view.setVisibility(View.GONE);
        }
    }



}
